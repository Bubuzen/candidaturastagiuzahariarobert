import java.util.Scanner;
public class Main {
	public static void main(String[]Arg){
		Scanner in=new Scanner(System.in);
		int OK=1;
		while(OK==1){                                     //This gives the possibility to pick another object
		System.out.println("Choose the geometric figure:");
		System.out.println("1-Circle");
		System.out.println("2-Triangle");
		System.out.println("3-Rectangle");
		int n = in.nextInt();
		if(n==1)
			{
			System.out.println("Input the radius of the circle r=");
			double r=in.nextDouble();
			Circle x=new Circle(r);
			x.Display();
			}
		else
			if(n==2){
				System.out.println("Input side 1 of the triangle s1=");
				double s1=in.nextDouble();
				System.out.println("Input side 2 of the triangle s2=");
				double s2=in.nextDouble();
				System.out.println("Input the hypotenuse of the triangle hypo=");
				double hypo=in.nextDouble();
				Triangle x=new Triangle(s1,s2,hypo);
				x.Display();
			}
			else
				if(n==3)
			{
				System.out.println("Input the lenght of the rectangle leng=");
				double Leng=in.nextDouble();
				System.out.println("Input the width of the rectangle wid=");
				double wid=in.nextDouble();
				Rectangle x=new Rectangle(Leng,wid);
				x.Display();
			}
				else
					System.out.println("This option does not exist !");
		System.out.println("Do you wish to choose another geometric figure?");
		System.out.println("1-Yes!");
		System.out.println("2-No!");
		OK=in.nextInt();
		}	
	}
}
