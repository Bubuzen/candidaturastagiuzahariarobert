
public class Rectangle {
	private double leng;
	private double width;
	public Rectangle(){			//Calls the rectangle constructor without arguments
		leng=0;
		width=0;
	}
	public Rectangle(double x,double y){   //Calls the rectangle constructor with arguments
		leng=x;
		width=y;
	}
	public double Area(){				//Calculates and returns the area of the rectangle
		double A=leng*width;
		return A;
	}
	public double Perimeter(){			//Calculates and returns the perimeter of the rectangle
		double P=2*leng+2*width;
		return P;
	}
	public void Display(){												//Displays the radius as well as the results
		System.out.println("Lungimea dreptunghiului este:"+leng);
		System.out.println("Latimea dreptunghiului este:"+width);
		this.Area();
		System.out.println("Area dreptunghiului este:"+Area());
		this.Perimeter();
		System.out.println("Perimetrul dreptunghiului este:"+Perimeter());
		}
}
