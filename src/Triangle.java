
public class Triangle {
	private double c1;
	private double c2;
	private double ip;
	public Triangle(){   //Calls the triangle constructor without arguments
		c1=0;
		c2=0;
		ip=0;
	}
	public Triangle(double x,double y,double z){    //Calls the triangle constructor with arguments
		c1=x;
		c2=y;
		ip=z;
	}
	public double Area(){				//Calculates and returns the area of the triangle
		double p=(c1+c2+ip)/2;
		double A=Math.sqrt(p*(p-c1)*(p-c2)*(p-ip));
		return A;
	}
	public double Perimeter(){			//Calculates and returns the perimeter of the triangle
		double P=c1+c2+ip;
		return P;
	}
	public void Display(){												//Displays the radius as well as the results
		System.out.println("Adjacent side of the triangle is:"+c1);
		System.out.println("Opposite side of the triangle is:"+c2);
		System.out.println("Hypotenuse of the triangle is:"+ip);
		this.Area();
		System.out.println("Area of the triangle is :"+Area());
		this.Perimeter();
		System.out.println("Perimetre of the triangle is:"+Perimeter());
		}
	
}
