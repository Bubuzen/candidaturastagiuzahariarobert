
public class Circle {
	private double radius;
	//
	public Circle(){      //Calls the circle constructor without arguments
		radius=0;
	}
	public Circle(double x){    // Calls the circle constructor with arguments
		radius = x;
	}
	public double Area(){        //Calculates and returns the area of the circle
		double pi=3.14;
		double A=radius*radius+pi;
		return A;
	}
	public double Perimeter(){   //Calculates and returns the perimeter of the circle
		double pi=3.14;
		double P=2*pi*radius;
		return P;
	}
	
	public void Display(){									//Displays the radius as well as the results
		System.out.println("The radius of the circle is:"+radius);
		this.Area();
		System.out.println("Area of the circle is:"+Area());
		this.Perimeter();
		System.out.println("Perimetre of the circle is:"+Perimeter());
	}
}
